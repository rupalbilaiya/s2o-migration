
# Sybase To Oracle Migration (Python)

The requirement in Sybase to Oracle migration was to convert all stored procedure and table ddls from sybase to PL SQL. Scratch editor converts 60-70% of the sql code from different sources to PL SQL. Another 20-30% are standard changes required as per requirements and remaining will need some manual changes to get the code to work. To reduce this 30-40% manual effort on the scratch editor converted version, a POC was used. Python was used to get the input ddl from Sybase, to convert code using sdcli.exe, do some standard changes, to fix common errors in the code, and execute the output in Oracle using sqlPlus. The goal was to reduce the manual efforts to 10% or less.


## Authors

- [@Rupal Bilaiya](rupal.bilaiya@gmail.com)



## Acknowledgements

 - Regex python (https://docs.python.org/3/library/re.html)
 - SQLPlus (https://docs.oracle.com/cd/B25329_01/doc/appdev.102/b25108/xedev_sqlplus.htm)
 - Connecting to SQLPlus with python (https://stackoverflow.com/questions/13568014/can-i-use-sqlplus-to-execute-a-sql-file#:~:text=from%20subprocess%20import%20Popen%2C%20PIPE%20%23function%20that%20takes,stdin%3DPIPE%2C%20stdout%3DPIPE%2C%20stderr%3DPIPE%29%20session.stdin.write%20%28sqlCommand%29%20return%20session.communicate%20%28%29)


## Tech Stack

**Language:** Python, PL/SQL

**Tools:** Pycharm, SQLPlus


## Code Description

#### Input file:
- Connection String.csv
This file contains connection details for Sybase and Oracle.

#### Output files:
- Sybase codes
DDLs for stored procedure and tables are fetched from Sybase and stored in 'Input' folder.

- status.csv
This file is created under logs folder. It will be updated through out the code with respective status for sql code.

- Staging files
Output from scdli.exe are stored in staging folder.

- Python converted files
Files are being processed through python are stored in output folder.

#### Code Flow:

- Sybase DDL input files are given as input to scdli.exe to run it across scratch editor.
- Using rules specified in regexForUtils.cfg, the output files from scdli.exe are updated using regex.
- Connection with SQLPlus is established.
- All sql files present in output folder are executed in Oracle and the corresponding status is noted in status file.



## Documentation

[GITLAB Project](https://gitlab.com/rupalbilaiya/s2o-migration)

