# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 17:35:27 2019

@author: krishna.rangu
        rupal.bilaiya
"""

import os
import glob
import re
import pandas as pd
import sys
from subprocess import Popen, PIPE
import pymssql
import warnings

warnings.filterwarnings("ignore")

user_input = sys.argv[1]
code_folder = sys.argv[2]
file_folder = sys.argv[3]

#------------------Read connection string excel file-----------------#
conn_sheet = pd.read_csv(file_folder + '\\conf\\Connection String.csv')
connection_dict = conn_sheet.to_dict(orient='record')

#---------------------Connect to Sybase and get DDL-------------------#

if user_input.upper() == 'Y':
    for connection_row in connection_dict:
        conn = pymssql.connect(server=str(connection_row['Source_server']), user=str(connection_row['Source_userid']),
                               password=str(connection_row['Source_password']), database=str(connection_row['Source_DB']),
                               port=str(connection_row['Source_port']), conn_properties='')
        db_connection = conn.cursor()
        object_name = connection_row['Object_name']
        object_type = connection_row['Object_type']
        schema_name = connection_row['Source_schema']

        if object_type == 'Table':

            db_connection.execute("COMMIT")

            try:
                db_connection.execute("sp_helpconstraint \'" + object_name + "\', detail")
                cons = db_connection.fetchall()
            except pymssql.OperationalError:
                cons = []

            list_foreign = []
            for defi in cons:
                if 'referential constraint' == defi[1]:
                    colreg = r'(' + object_name + ' )(\s*FOREIGN\s+KEY\s*\()(.*\))(\s*.*)(\s+REFERENCES\s+.*)'
                    colstr = re.compile(colreg, re.IGNORECASE | re.MULTILINE)
                    dropProcs = re.finditer(colstr, defi[2])
                    if (dropProcs):
                        for colfind in dropProcs:
                            list_foreign.append('ALTER TABLE ' + object_name + ' ADD ' + colfind.group(2) + colfind.group(3) + colfind.group(5))

            conn = pymssql.connect(server=str(connection_row['Source_server']),
                                   user=str(connection_row['Source_userid']),
                                   password=str(connection_row['Source_password']),
                                   database=str(connection_row['Source_DB']),
                                   port=str(connection_row['Source_port']), conn_properties='')

            db_connection = conn.cursor()

            db_connection.execute("COMMIT")

            table_ddl_sql = """DECLARE @TableName               varchar(50)
               DECLARE @ObjectID                int
               DECLARE @IndexID                 int
               DECLARE @IndexStatus             int
               DECLARE @IndexName               varchar(30)
               DECLARE @msg                     varchar(255)
               DECLARE @OnlyTableName           varchar(50)
               DECLARE @LastColumnId            int
               DECLARE @i                       int


               SELECT @OnlyTableName = \'""" + object_name + """\'


               CREATE TABLE #columns (
                  column_name char(30)    NULL,
                  type_name   char(30)    NULL,
                  length      char(10)    NULL,
                  iden_flag   char(10)    NULL,
                  null_flag   char(255)    NULL,
                  flag        char(1)     NULL,
                  default_value char(255)  NULL
               )

               CREATE TABLE #rtn (
                  msg   varchar(255)   NULL
               )


               SELECT @TableName = obj.name,
                      @ObjectID  = obj.id
                 FROM sysobjects obj
                 JOIN sysusers users
                 ON users.uid = obj.uid
                 AND users.name = \'""" + schema_name + """\'
                WHERE obj.type = 'U'
                  AND obj.name = @OnlyTableName
                ORDER BY obj.name

               SELECT @LastColumnId = MAX(colid) FROM syscolumns WHERE id = @ObjectID

               INSERT #columns
               SELECT col.name,
                      typ.name,
                      CASE WHEN typ.name in ('decimal','numeric') THEN '(' +
            convert(varchar, col.prec) + ',' + convert(varchar, col.scale) + ')'
                           WHEN typ.name like '%char%'THEN
            '('+CONVERT(varchar,col.length)+')'
                           ELSE '' END,
                      CASE WHEN col.status = 0x80 THEN 'IDENTITY' ELSE '' END,
                      CASE WHEN convert(bit, (col.status & 8)) = 0 THEN "NOT NULL"
            ELSE "NULL" END + CASE WHEN col.colid = @LastColumnId THEN ' )' ELSE
            ',' END,
                      NULL,
                      (select  com.text 
                      from   syscolumns  syscol,
                            syscomments com
            where  syscol.id = object_id(@TableName)
            and syscol.name = col.name
            and    syscol.cdefault = com.id)
                 FROM syscolumns col, systypes typ
                WHERE col.id = @ObjectID
                  AND col.usertype = typ.usertype
                ORDER BY col.colid



               INSERT #rtn
               SELECT "CREATE TABLE " + @TableName + " ("
               UNION ALL
               SELECT '    '+
                                column_name + replicate(' ',30- len(column_name)) +
                                type_name + length + replicate(' ',20 -
            len(type_name+length)) +
                                iden_flag + replicate(' ',10 - len(iden_flag)) + default_value + ' ' +
                                null_flag
                    FROM #columns

               SELECT name, indid, status, 'N' as flag INTO #indexes
               FROM sysindexes WHERE id = @ObjectID

               SET ROWCOUNT 1
               WHILE 1=1
               BEGIN
                  SELECT @IndexName = name, @IndexID = indid, @IndexStatus =
            status FROM #indexes WHERE flag = 'N'
                  IF @@ROWCOUNT = 0
                     BREAK

                  SELECT @i = 1
                  SELECT @msg = ''
                  WHILE 1=1
                  BEGIN
                     IF index_col(@TableName, @IndexID, @i) IS NULL
                        BREAK

                     SELECT @msg = @msg + index_col(@TableName, @IndexID, @i) +
            CASE WHEN index_col(@TableName, @IndexID, @i+1) IS NOT NULL THEN ','
            END
                     SELECT @i = @i+1
                  END

                  IF @IndexStatus & 2048 = 2048 --PRIMARY KEY
                     INSERT #rtn
                     SELECT "ALTER TABLE " + @TableName +
                            " ADD CONSTRAINT " + @IndexName +
                            " primary key "+
                            CASE WHEN @IndexID != 1 THEN 'nonclustered ' END +
            '('+ @msg +')'
                  ELSE
                     IF (@IndexStatus & 2048 = 0 AND @IndexID NOT IN (0, 255))
            --NOT PRIMARY KEY
                        INSERT #rtn
                        SELECT 'CREATE '+
                        CASE WHEN @IndexStatus & 2 = 2 THEN 'UNIQUE ' ELSE '' END +
                        CASE WHEN @IndexID = 1 THEN 'CLUSTERED ' ELSE 'NONCLUSTERED ' END +
                        'INDEX ' + @IndexName + ' ON ' + @TableName + ' ('+ @msg +')'

                  UPDATE #indexes SET flag = 'Y' WHERE indid = @IndexID
                  
                  select text as "constrainttext"
            into #checkCons
            from sysconstraints,syscomments
            where sysconstraints.status=128 and 
            sysconstraints.constrid=syscomments.id
            and object_name(tableid) = @TableName

            INSERT #rtn
             SELECT "ALTER TABLE " + @TableName + " ADD " + constrainttext
             from #checkCons

               END
               SET ROWCOUNT 0"""

            db_connection.execute(table_ddl_sql)
            db_connection.execute("SELECT * FROM #rtn")
            result2 = db_connection.fetchall()
            # print(result2)
            resultlen = len(result2)
            result3 = ''
            for i in range(resultlen):
                for j in result2[i]:
                    result3 = result3 + '\n' + j

            my_file = open(file_folder + '\\input\\' + object_name.upper() + ".SQL", "w")
            my_file.write(result3)
            for foreign in list_foreign:
                my_file.write('\n' + foreign + '\n')
            my_file.close()

        else:

            sql_statement = """SELECT com.text 
                                FROM sysobjects obj 
                                JOIN syscomments com 
                                ON obj.id = com.id 
                                JOIN sysusers users
                                ON users.uid = obj.uid
                                AND users.name = \'""" + schema_name + """\'
                                WHERE obj.name = \'""" + object_name + "\'"""
                #"SELECT com.text FROM sysobjects obj JOIN syscomments com ON obj.id = com.id WHERE obj.name = \'" + object_name + "\'"
            result = db_connection.execute(sql_statement)
            result = db_connection.fetchall()
            if result == None:
                print(object_name + ' doesn\'t exists')
                continue
            resultlen = len(result)
            result1 = ''
            for i in range(resultlen):
                for j in result[i]:
                    result1 = result1 + j

            my_file = open(file_folder + '\\input\\' + object_name.upper() + ".SQL", "w")
            my_file.write(result1)
            my_file.close()

#------------------To start auditing input file's status-------------------#
inputvalue=[]
emptylist = []
for (dirpath, dirnames, filenames) in os.walk(file_folder+'\\input\\'):
    len1 = len(filenames)
    for filelenrange in range(len1):
        filenames[filelenrange] = filenames[filelenrange].upper()
        inputvalue.append('Migration Started')
        emptylist.append('Not Applicable')

df = pd.DataFrame({'Object_Name': filenames, 'Status': inputvalue, 'Compiled(Y/N)': emptylist})
df.to_csv(file_folder+'\\logs\\status.csv')

#-----------------Run scdli.exe tool for all the input file present-------------------#
os.system('cmd /c '+code_folder+'\\sqldeveloper-19.2.1.247.2212-x64\\sqldeveloper\\sqldeveloper\\bin\\sdcli migration -actions=translate -dir='+file_folder+'\\input -translator=sybase -output='+file_folder+'\\staging -ext=.SQL -r')

#------------------Audit Migration COmpleted-------------------#
df['Status'] = df['Status'].replace('Migration Started', 'Migration Completed')
df.to_csv(file_folder+'\\logs\\status.csv')

#-----------------Read some configuration files-------------------#
regdf = pd.read_csv(code_folder+"\\bin\\regexForUtils.cfg", sep='\t', header='infer')
pd.set_option('display.max_colwidth', -1)

#-----------------Function defination used as part of FACTORY MODEL-------------------#
def rreplace(s, old, new):
	return (s[::-1].replace(old[::-1],new[::-1], 1))[::-1]

def getPatternMatches(rscript, regdf, seq):
    reg = str(regdf.loc[regdf.no == seq, 'regex'].values[0])
    string = re.compile(reg, re.IGNORECASE)
    matches = re.findall(string,rscript)
    index = getIndex(string,rscript)
    return matches, index

def pragmaFunction(rscript, logf):
    pragmareg = r'(CREATE|CREATE OR REPLACE) FUNCTION'
    pragmastr = re.compile(pragmareg, re.IGNORECASE)
    pragmas = re.search(pragmastr, rscript)
    if(pragmas):
        pragmas = pragmas.start()
        pragmaBeginreg = r'BEGIN'
        pragmaBeginstr = re.compile(pragmaBeginreg, re.IGNORECASE)
        pragmaBegins = re.findall(pragmaBeginstr, rscript)
        pragmaBeginIndex = pragmaBeginstr.search(rscript,pragmas)

        if (pragmaBegins):
            for pragmaBegin in pragmaBegins:
                lineNumberFunction(1, pragmaBeginIndex.span()[0], pragmaBegin, 'PRAGMA AUTONOMOUS_TRANSACTION; \n BEGIN', logf,rscript)
                rscript = rscript[0:pragmaBeginIndex.span()[0]] + rscript[pragmaBeginIndex.span()[0]:pragmaBeginIndex.span()[
                1]].replace(pragmaBegin, 'PRAGMA AUTONOMOUS_TRANSACTION; \n BEGIN') \
                     + rscript[pragmaBeginIndex.span()[1]:len(rscript)]
                
                break;
        else:
            rscript = rscript

    return rscript

def SETCOUNTFunction(script,logf):
    SETCOUNTregex = r'\bset\s+nocount\s+on\b'
    SETCOUNTstr = re.compile(SETCOUNTregex,re.IGNORECASE)
    SETCOUNTFinditer = re.finditer(SETCOUNTstr, script)

    if (SETCOUNTFinditer):
        for SETCOUNTvalue in SETCOUNTFinditer:
            lineNumberFunction(33, SETCOUNTvalue.span()[0], SETCOUNTvalue.group(0), 'set feedback off',logf,script)
            script = script.replace(SETCOUNTvalue.group(0), 'set feedback off')
    return script;

def ExecuteImmediateFunction(script, logf):
    ExecuteImmediatereg = r'(EXECUTE\s+IMMEDIATE\s+)(\'([^\']*)\')'#r'(EXECUTE\s+IMMEDIATE\s+)(\'[\w+\s+.*\\n*]+\')'#r'(EXECUTE\s+IMMEDIATE\s*)(\'\s*\w+\')(\s*\;)'
    ExecuteImmediatestr = re.compile(ExecuteImmediatereg, re.IGNORECASE|re.MULTILINE)
    # ExecuteImmediatestr = re.compile(ExecuteImmediatestr, )
    ExecuteImmediates = re.finditer(ExecuteImmediatestr, script)

    if (ExecuteImmediates):
        for ExecuteImmediate in ExecuteImmediates:
            statement = ExecuteImmediate.group(0).split('\'')
            lineNumberFunction(13, ExecuteImmediate.span()[0], ExecuteImmediate.group(0), 'DBMS_UTILITY.EXEC_DDL_STATEMENT (\'' + statement[1] + '\')',logf,script)
            script = script.replace(ExecuteImmediate.group(0), 'DBMS_UTILITY.EXEC_DDL_STATEMENT (\'' + statement[1] + '\')')

    return script    

def GTTNameFunction(rscript,logf):
    GTTNamereg = r'(tt_\w+)([\s|\\n|;|\W]?)'
    GTTNamestr = re.compile(GTTNamereg, re.IGNORECASE)
    GTTNameall = re.findall(GTTNamestr, rscript)
    sampleSet = set()

    if (GTTNameall):

        for tupleValue in GTTNameall:
            sampleSet.add(tupleValue[0])

        for setValue in sampleSet:
            str = setValue.strip(setValue[:2])
            if setValue[-5:].upper() != '_TEMP':
                rscript = rscript.replace(setValue, 'GTT' + str+'_TEMP')
            else:
                rscript = rscript.replace(setValue, 'GTT' + str)

    GTTNameiter = re.finditer(GTTNamestr, rscript)
    if (GTTNameiter):
        for logFile in GTTNameiter:
            
            str = logFile.group(0).strip(logFile.group(0)[:2])
            if logFile.group(0)[-5:].upper() != '_TEMP':
                codeSnip = 'GTT' + str.strip()+'_TEMP'
            else:
                codeSnip = 'GTT' + str.strip()
            lineNumberFunction(31,  logFile.span()[0], logFile.group(0).strip(), codeSnip, logf,rscript)
            

    return rscript


def lineNumberFunction(wikino,index,before_str, after_str, logf,rscript):
    old_string = ''.join(before_str)
    lineno = rscript.count('\n', 0, index) + 1
    line = '{0}\t{1}\t"{2}"\t"{3}"\n'.format(wikino,lineno,old_string,after_str.replace('\n',''))
    logf.write(line)

def replaceLRtrims(rscript, logf):
    lrTrims,index = getPatternMatches(rscript,regdf, 1)
    j = 0
    if(lrTrims):
        for lrTrim in lrTrims:
            
            line = lrTrim[0]+lrTrim[1]+lrTrim[2]+lrTrim[3]+lrTrim[4]+lrTrim[5]
            codeSnip = 'TRIM('+lrTrim[4]+')'
            lineNumberFunction(32, index[j], lrTrim, codeSnip, logf,rscript)
            rscript  = rscript.replace(line,codeSnip)
            
            j=j+1
    else:
        rscript = rscript
    return rscript

def charFunction(rscript, logf):
    allChars, index = getPatternMatches(rscript,regdf, 26)
    #comp = r' char'
    #stext = re.compile(comp, re.IGNORECASE)
    #allChar = re.findall(stext, rscript)
    j = 0
    if(allChars):
        rscript = re.sub(' CHAR',' VARCHAR2',rscript,flags = re.I)
        for allChar in allChars:
            lineNumberFunction(41, index[j], allChar, ' VARCHAR2', logf,rscript)
            j=j+1
    else:
        rscript = rscript
    return rscript

def replaceDeleteIdentity(rscript, logf):
    delIds, index = getPatternMatches(rscript,regdf, 3)
    j = 0
    if(delIds):
        for delId in delIds:
            codeSnip = ""
            lineNumberFunction(42,index[j], delId, codeSnip, logf,rscript)
            rscript = rscript.replace(delId[0]+delId[1]+delId[2]+delId[3], codeSnip)
            
            j=j+1
    else:
        rscript = rscript
    return rscript;

def replacePlusinOrderBy(rscript, logf):
    orders, index = getPatternMatches(rscript,regdf, 12)
    j = 0
    if(orders):
        for order in orders:
            if('UTILS.' not in order[1] and '+' in order[1]):
                rcols = []
                cols = order[1].split(",")
                for col in cols:
                    mcol = ''
                    if('+' in col.strip()):
                        mcol = col.replace('+', '|')
                        col = mcol
                    rcols.append(col)
                codeSnip = ','.join(rcols)
                lineNumberFunction(43, index[j], order, order[0]+' '+codeSnip, logf,rscript)
                rscript = rscript.replace(order[1], codeSnip)
                
                j=j+1
    else:
        rscript = rscript
    return rscript

def replacePlusinWhere(rscript, logf):
    wheres, index = getPatternMatches(rscript,regdf, 13)
    j = 0
    if(wheres):
        for where in wheres:
            if('UTILS.' not in where[1] and '+' in where[1]):
                rcols = []
                mcol = where[1]
                if('+' in where[1].strip()):
                    mcol = where[1].replace('+', '|')
                rcols.append(mcol)
                codeSnip = where[0]+' '+','.join(rcols)
                lineNumberFunction(43, index[j], where, codeSnip, logf,rscript)
                rscript = rscript.replace(where[0]+where[1], codeSnip)
                
                j=j+1
    else:
        rscript = rscript
    return rscript
def replaceSelect21(rscript, logf):
    sel21s, index = getPatternMatches(rscript,regdf, 14)
    j = 0
    if(sel21s):
        for sel21 in sel21s:
            if('FROM DUAL' in sel21[1].strip().upper() and ' INTO ' not in sel21[1].strip().upper()):
                v_string = sel21[1].upper().split("FROM")[0]
                codeSnip = "BEGIN\n DBMS_OUTPUT.PUT_LINE("+v_string.strip()+");\n END;"
                lineNumberFunction(17, index[j], sel21, codeSnip, logf,rscript)
                rscript = rscript.replace(sel21[0]+sel21[1], codeSnip)
                
                j=j+1
            else:
                pass
    else:
        rscript = rscript
    return rscript


def getIndex(restr, rscript):
    i = 0
    index = {}
    for m in re.finditer(restr,rscript):
        index[i] = m.span()[0]
        i = i + 1
    return index

def replaceTransactionCalls(rscript, logf):
    rollbacks, index = getPatternMatches(rscript,regdf, 17)
    j = 0
    if (rollbacks):
        for rollback in rollbacks:
            rollback = rollback[0].strip()+' '+rollback[1]
            if(rollback.upper()=='ROLLBACK TRANSACTION'):
                lineNumberFunction(8, index[j], rollback, 'ROLLBACK', logf,rscript)
                rscript = rscript.replace(rollback, 'ROLLBACK')
                
                j=j+1
            elif(rollback.upper()=='COMMIT TRANSACTION'):
                lineNumberFunction(9, index[j], rollback, 'COMMIT', logf,rscript)
                rscript = rscript.replace(rollback, 'COMMIT')
                
                j=j+1
    else:
        rscript = rscript

    
    utilTrans, index = getPatternMatches(rscript,regdf, 18)
    j = 0
    if (utilTrans):
        for utilTran in utilTrans:
            if(utilTran.upper()=='UTILS.COMMIT_TRANSACTION'):
                lineNumberFunction(9, index[j], utilTran, 'COMMIT', logf,rscript)
                rscript = rscript.replace(utilTran, 'COMMIT')
                
                j=j+1
            else:
                lineNumberFunction(42, index[j], utilTran, '', logf,rscript)
                rscript = rscript.replace(utilTran, '')
                
                j=j+1
    else:
        rscript = rscript

    return rscript


def replaceNulls(rscript, logf):
    NULLs, index = getPatternMatches(rscript,regdf, 19)
    j = 0
    if (NULLs):
        for NULL in NULLs:
            NULL = NULL[0].strip()+' '+NULL[1]
            if(NULL.upper()=='<> NULL'):
                lineNumberFunction(14, index[j], NULL, 'IS NOT NULL', logf,rscript)
                rscript = rscript.replace(NULL, 'IS NOT NULL')
                
                j=j+1
            elif(NULL.upper()=='= NULL'):
                lineNumberFunction(14, index[j], NULL, 'IS NULL', logf,rscript)
                rscript = rscript.replace(NULL, 'IS NULL')
                
                j=j+1
            else:
                rscript = rscript
                
    else:
        rscript = rscript

    return rscript

def replaceStringReplaceFunction(rscript, logf):
    strReplaces, index = getPatternMatches(rscript,regdf, 20)
    j = 0
    if (strReplaces):
        for strReplace in strReplaces:
            lineNumberFunction(12, index[j], strReplace, 'REPLACE', logf,rscript)
            rscript = rscript.replace(strReplace, 'REPLACE')
            
            j=j+1
    else:
        rscript = rscript

    return rscript

def replacesysTable(rscript, logf):
    sysColumns, index = getPatternMatches(rscript,regdf, 21)
    j = 0
    if (sysColumns):
        for sysColumn in sysColumns:
            if(sysColumn=='syscolumns'):
                lineNumberFunction(29, index[j], sysColumn, 'user_tab_cols', logf,rscript)
                rscript = rscript.replace(sysColumn, 'user_tab_cols')
                
                j=j+1
            elif(sysColumn=='sysobjects'):
                lineNumberFunction(29, index[j], sysColumn, 'all_objects', logf,rscript)
                rscript = rscript.replace(sysColumn, 'all_objects')
                
                j=j+1
            elif(sysColumn=='sysusers'):
                lineNumberFunction(29, index[j], sysColumn, 'all_users', logf,rscript)
                rscript = rscript.replace(sysColumn, 'all_users')
                
                j=j+1
    else:
        rscript = rscript

    return rscript

def SYSDATEFunction(rscript,logf):
    SYSDATEregex = r'\bSYSDATE\b'
    SYSDATEstr = re.compile(SYSDATEregex,re.IGNORECASE)
    SYSDATEFinditer = re.finditer(SYSDATEstr, rscript)

    if SYSDATEFinditer:
        for SYSDATAvalue in SYSDATEFinditer:
            lineNumberFunction(11, SYSDATAvalue.span()[0], SYSDATAvalue.group(0), 'SYSTIMESTAMP', logf,rscript)
            rscript = rscript.replace(SYSDATAvalue.group(0), 'SYSTIMESTAMP')
            
            
    return rscript;

def replaceStatistics(rscript, logf):
    statistics, index = getPatternMatches(rscript,regdf, 25)
    j = 0
    if(statistics):
        for stat in statistics:
            lineNumberFunction(28, index[j], stat, '', logf,rscript)
            rscript = rscript.replace(stat, 'UPDATE DBMS_STATS.GATHER_TABLE_STATS ')
            
            j=j+1
    else:
        rscript = rscript
    return rscript

def missedWiki(lineno,counter,msg,wikif,wikino):
    counter = counter + 1
    wikif.write('{0}. {1} {2}. Please refer Wiki#{3}.\n'.format(counter,msg,lineno,wikino))
    return counter

def multiDelete(rscript, counter, wikif):
    delReg = '(DELETE\s+FROM\s+)(.*)(\\n+|\s+)(DELETE\s+FROM\s+)'
    delStr = re.compile(delReg, re.I)
    delsts = re.findall(delStr, rscript)
    index = getIndex(delStr, rscript)
    j = 0
    if(delsts):
        msg = "There are MULTIPLE DELETE statements at line:"
        for delst in delsts:
            lineno = rscript.count('\n', 0, index[j]) + 1
            counter = missedWiki(lineno,counter,msg,wikif,3)
            j = j + 1
    return counter
    
def findDate(rscript, counter, wikif):
    dateReg = r'(\bDATE\b)(\,|\\n|\s)'
    dateStr = re.compile(dateReg)
    datests = re.findall(dateStr, rscript)
    index = getIndex(dateStr, rscript)
    j = 0
    if(datests):
        msg = "DATE datatype column is present at line:"
        for datest in datests:
            lineno = rscript.count('\n', 0, index[j]) + 1
            counter = missedWiki(lineno,counter,msg,wikif,11)
            j = j + 1
    return counter
def spRecompile(rscript, counter, wikif):
    spRecomreg = '(sp_recompile\s+)'
    spRecomstr = re.compile(spRecomreg, re.I)
    spRecomsts = re.findall(spRecomstr, rscript)
    index = getIndex(spRecomstr, rscript)
    j = 0
    if(spRecomsts):
        msg = 'sp_recompile keyword is present at line:'
        for spRecomst in spRecomsts:
            lineno = rscript.count('\n', 0, index[j]) + 1
            counter = missedWiki(lineno,counter,msg,wikif,27)
            j = j + 1
    return counter

def sysMessages(rscript, counter, wikif):
    symMsgreg = '(sysmessages)'
    symMsgstr = re.compile(symMsgreg, re.I)
    symMsgsts = re.findall(symMsgstr, rscript)
    index = getIndex(symMsgstr, rscript)
    j = 0
    if(symMsgsts):
        msg = 'sysmessages keyword is present at line:'
        for symMsgst in symMsgsts:
            lineno = rscript.count('\n', 0, index[j]) + 1
            counter = missedWiki(lineno,counter,msg,wikif,24)
            j = j + 1
    return counter

def inOutParam(rscript, counter, wikif):
    inOutreg = '(IN\s+OUT\s+)'
    inOutstr = re.compile(inOutreg, re.I)
    inOutsts = re.findall(inOutstr, rscript)
    index = getIndex(inOutstr, rscript)
    j = 0
    if(inOutsts):
        msg = 'IN OUT parameter is present at line:'
        for inOutst in inOutsts:
            lineno = rscript.count('\n', 0, index[j]) + 1
            counter = missedWiki(lineno,counter,msg,wikif,35)
            j = j + 1
    return counter

def groupBy(rscript, counter, wikif):
    groupByreg = '(GROUP\s+BY\s+)'
    groupBystr = re.compile(groupByreg, re.I)
    groupBysts = re.findall(groupBystr, rscript)
    index = getIndex(groupBystr, rscript)
    j = 0
    if(groupBysts):
        msg = 'GROUP BY keyword is present at line:'
        for groupByst in groupBysts:
            lineno = rscript.count('\n', 0, index[j]) + 1
            counter = missedWiki(lineno,counter,msg,wikif,36)
            j = j + 1
    return counter


def alterTable(rscript, counter, wikif):
    alterTabreg = '(ALTER\s+TABLE)'
    alterTabstr = re.compile(alterTabreg, re.I)
    alterTabsts = re.findall(alterTabstr, rscript)
    index = getIndex(alterTabstr, rscript)
    j = 0
    if(alterTabsts):
        msg = 'ALTER TABLE keyword is present at line:'
        for alterTabst in alterTabsts:
            lineno = rscript.count('\n', 0, index[j]) + 1
            counter = missedWiki(lineno,counter,msg,wikif,44)
            j = j + 1
    return counter

def regexpLIKE(rscript, counter, wikif):
    regexp = '(REGEXP_LIKE\s+)'
    regexpstr = re.compile(regexp, re.I)
    regexpsts = re.findall(regexpstr, rscript)
    index = getIndex(regexpstr, rscript)
    j = 0
    if(regexpsts):
        msg = 'REGEXP_LIKE keyword is present at line:'
        for regexpst in regexpsts:
            lineno = rscript.count('\n', 0, index[j]) + 1
            counter = missedWiki(lineno,counter,msg,wikif,45)
            j = j + 1
    return counter

def identitySeq(rscript, counter, wikif):
    idreg = '(identity\(\d+\))'
    idregstr = re.compile(idreg, re.I)
    idregsts = re.findall(idregstr, rscript)
    index = getIndex(idregstr, rscript)
    j = 0
    if(idregsts):
        msg = 'IDENTITY() function is present at line:'
        for idregst in idregsts:
            lineno = rscript.count('\n', 0, index[j]) + 1
            counter = missedWiki(lineno,counter,msg,wikif,40)
            j = j + 1
    return counter

def concat(rscript, counter, wikif):
    concatreg = '(\s*\|\|\s*)'
    concatstr = re.compile(concatreg, re.I)
    concatsts = re.findall(concatstr, rscript)
    index = getIndex(concatstr, rscript)
    j = 0
    if(concatsts):
        msg = '+ is replaced with || by scratch editor at line:'
        for concatst in concatsts:
            lineno = rscript.count('\n', 0, index[j]) + 1
            counter = missedWiki(lineno,counter,msg,wikif,43)
            j = j + 1
    return counter

def wikiFunctions(rscript, counter, wikif):
    cnt1 = multiDelete(rscript, counter, wikif)
    # cnt2 = findDate(rscript, cnt1, wikif)
    cnt3 = spRecompile(rscript, cnt1, wikif)
    cnt4 = sysMessages(rscript, cnt3, wikif)
    cnt5 = inOutParam(rscript, cnt4, wikif)
    cnt6 = groupBy(rscript, cnt5, wikif)
    cnt7 = alterTable(rscript, cnt6, wikif)
    cnt8 = regexpLIKE(rscript, cnt7, wikif)
    cnt9 = identitySeq(rscript, cnt8, wikif)
    cnt10 = concat(rscript, cnt9, wikif)

# Adds alter session statement to the input script if not present    
def addHeader(rscript, logf):
    header, index = getPatternMatches(rscript,regdf, 28)
    header_curr_schema = 'ALTER SESSION SET CURRENT_SCHEMA=SCHEMA_NAME;\n'
    if not header:
        rscript = header_curr_schema + rscript
        lineNumberFunction(7, 1, 'current_schema', header_curr_schema, logf,rscript)
    return rscript

# Remove comments from input script
# Used as help function in inOutParamName for renaming input output variables 
# As comments were used within the parameters parenthesis
def removeComments(parameterContent, index, logf, rscript):
    regex_comment= r"(\/\*.*?\*\/)"
    comment_rgx = re.compile(regex_comment, re.DOTALL)
    comments = re.findall(comment_rgx, parameterContent)
    if comments:
        j=0
        commentsIndex = getIndex(comment_rgx,parameterContent)
        for comment in comments:
            parameterContent = parameterContent.replace("".join(comment),'')
            lineNumberFunction(35, index+commentsIndex[j], r'Parameters', 'Removed comments', logf, rscript)
            j=j+1
    return parameterContent

# Changes variable names as provided in the input list throughout the script
# Used as help function in inOutParamName
# Renamed parameters has to be changed throughout the script
def changeVariableNames(rscript, variableList, logf):
    for variable in variableList:
        srcVar = variable[0]
        tarVar = variable[1]
        srcVariableRegex = re.compile(r"\b{}\b".format(srcVar), re.IGNORECASE)
        srcVariableIndex = getIndex(srcVariableRegex, rscript)
        rscript = srcVariableRegex.sub(tarVar,rscript)
        for index in srcVariableIndex:
            lineNumberFunction(35, srcVariableIndex[index], srcVar, tarVar, logf,rscript)
    return rscript

# Modified parameters naming convention based on input or output parameter
def inOutParamName(rscript, logf):
    procedureParameterRegex = r"(CREATE\s+)(OR\s+REPLACE\s+)?(PROCEDURE\s+)(\w*\.)?(\w+)\s*\((.*?)\)\s*AS"
    split_regex = r"\,(?!\w|\')"
    parameter_regex = re.compile(r"^v_", re.IGNORECASE)
    procedureParamsRegex = re.compile(procedureParameterRegex, re.IGNORECASE | re.DOTALL)
    #Used findall instead of predefined function, because this needed DOTALL flag
    inOutParams= re.findall(procedureParamsRegex, rscript)
    inOutParamsIndex = getIndex(procedureParamsRegex,rscript)
    changedVariableList=[]
    if inOutParams:
        for inOutParameter in inOutParams:
            inOutParameters = removeComments(inOutParameter[5], inOutParamsIndex[0], logf, rscript)
            parameters = [words.strip() for words in re.split(split_regex, inOutParameters)]
            for parameter in parameters:
                params, ind = getPatternMatches(parameter, regdf, 34)
                if(params):
                    src_variable =params[0]
                    if " OUT " in parameter.upper():
                        if parameter_regex.match(src_variable):
                            final_variable = re.sub(parameter_regex, "v_out_", src_variable)
                        else:
                            final_variable = "v_out_" + src_variable
                    else:
                        if parameter_regex.match(src_variable):
                            final_variable = re.sub(parameter_regex, "v_in_", src_variable)
                        else:
                            final_variable = "v_in_" + src_variable
                    changedVariableList.append([src_variable,final_variable])
            rscript = rscript.replace(''.join(inOutParameter[5]),inOutParameters)
    return changeVariableNames(rscript,changedVariableList, logf)

# Removes forward slash after create table script
def removeSlash(rscript, logf):
    createTablePattern = r"(CREATE\s*)(GLOBAL\s*)(TEMPORARY\s*)(TABLE\s*)(.*?\))(\s*\;\s*)(\/)"
    createTableRegex = re.compile(createTablePattern, re.IGNORECASE | re.DOTALL)
    slashIndex = getIndex(createTableRegex, rscript)
    rscript = createTableRegex.sub(r"\1\2\3\4\5\6", rscript)
    for index in slashIndex:
        lineNumberFunction(100, slashIndex[index], r"/", r"Removed /", logf,rscript)
    return rscript

# Adds Execute permission script
# Needs service account name in parameter 
def addFooter(rscript, logf, service_account_name):
    footer, index = getPatternMatches(rscript, regdf, 32)
    createProcedures, index = getPatternMatches(rscript, regdf, 30)
    if not footer and createProcedures:
        for createProcedure in createProcedures:
            footer_statement = '\n / \nGRANT EXECUTE ON {0} TO {1};'.format(createProcedure[4], service_account_name )
            rscript = rscript + footer_statement
            lineNumberFunction(100, len(rscript), 'Execute permission', footer_statement, logf, rscript)
    return rscript

def remove_consecutive_dup_del_stmts(sql_script, logf):
    pattern = r'[\n|\s]+delete.*?'
    rslt_sql_script = sql_script[ : ]
    del_stmts_start_end_pos = dict({((x.span()[1],re.sub(r'\s+','',x.group().replace('\n',''))), (x.group(), x.span()[0], x.span()[1])) for x in re.finditer(pattern, sql_script, re.I|re.DOTALL)})
    del_stmts_end_pos = dict([((v[1],k[1]), v[2]) for k,v in del_stmts_start_end_pos.items()])
    remove_del_stmts_pos = sorted([(k[0],v) for k,v in del_stmts_end_pos.items() if (k[0],k[1]) in del_stmts_start_end_pos], reverse=True)
    for i in remove_del_stmts_pos:       
        lineNumberFunction(3, i[0], rslt_sql_script[i[0]:i[1]], '', logf, rslt_sql_script)
        rslt_sql_script = rslt_sql_script[:i[0]] + rslt_sql_script[i[1]:]
    return rslt_sql_script

def enclose_sel_into(sql_script, logf):
    pattern = r'(select[^;]+into[^;]+;)'
    rslt_sql_script = sql_script[:]
    all_sel_into_str = [sel_into_str for sel_into_str in re.findall(re.compile(pattern, re.I),rslt_sql_script) if sel_into_str.count('/*') == sel_into_str.count('*/')]
    for sel_into in all_sel_into_str:
        rslt_sql_script = rslt_sql_script.replace(sel_into,'BEGIN \n  '+sel_into+'\nEXCEPTION \n  WHEN NO_DATA_FOUND THEN \n  NULL; \nEND; \n')
    return rslt_sql_script

def comment_sp_recompile(sql_script, logf):
    patterns = [r'(;[^;]+:=[ \n\']+sp_recompile[^;]+;+[^;]+EXECUTE[ \n]+IMMEDIATE[^;]+;)', r'([ \n]+EXECUTE[ \n]+IMMEDIATE[ \n\']+sp_recompile[^;]+;)']
    rslt_exec_sp = sql_script[:]
    for pattern in patterns:
        all_exec_sp = [ exec_sp for exec_sp in re.findall(re.compile(pattern, re.I),rslt_exec_sp)]
        for exec_sp in all_exec_sp:
            lineNumberFunction(27, 0, exec_sp[1:], '\n/*\n  '+exec_sp[1:].replace('*/', '* /')+'\n*/\n', logf, rslt_exec_sp)
            rslt_exec_sp = rslt_exec_sp.replace(exec_sp[1:],'\n/*\n  '+exec_sp[1:].replace('*/', '* /')+'\n*/\n')
    return rslt_exec_sp

def fix_identity_column_definition(sql_script, logf):
    pattern = r'([\s\n]+CREATE[\s\n]+SEQUENCE.*?;)'
    rslt_sql_script = sql_script[:]
    sequences = re.findall(pattern, rslt_sql_script, re.I|re.DOTALL)
    identity_table = dict()
    remove_identity_sequence = dict()
    remove_identity_trigger = dict()
    # for sequence in [list(filter(lambda x: x != '', seq.split(' ')))[2] for seq in sequences]:
    for sequence in sequences:
        sequence_name = list(filter(lambda x: x != '', sequence.split(' ')))[2]
        sequence_name = sequence_name.replace(';', '')
        triggers = re.findall(r'([\s\n]+CREATE[\s\n]+OR[\s\n]+REPLACE[\s\n]+TRIGGER.*?END;)', rslt_sql_script,
                              re.I | re.DOTALL)
        for trigger in triggers:
            if sequence_name in trigger:
                search_table = re.search(r'ON[\s\n]+(.*)FOR[\s\n]+', trigger, re.I | re.DOTALL)
                if search_table:
                    table = search_table.group(1).strip()
                    search_column = re.search(r'INTO[\s\n]+(.*)[\s\n]+FROM', trigger, re.I | re.DOTALL)
                    if search_column:
                        column = search_column.group(1).split('.')[1].strip(' \n')
                        identity_table[table] = column
                        remove_identity_trigger[table] = trigger
                        remove_identity_sequence[table] = sequence
    for table_type in ['TABLE', 'TEMPORARY[\n\s]+TABLE', 'GLOBAL[\s\n]+TEMPORARY[\n\s]+TABLE']:
        for table_name, column_name in identity_table.items():
            matched = re.findall(r'[\s\n]?CREATE[\s\n]' + table_type + '[\s\n]+' + table_name + '.*?;', rslt_sql_script,
                                 re.I | re.DOTALL)
            if matched:
                table_statement = matched[0]
                table_statement_updated = ','.join(list(map(
                    lambda x: x + ' GENERATED BY DEFAULT AS IDENTITY' if ' ' + column_name.upper().replace('\r',
                                                                                                  '') + ' ' in x.upper() and 'IDENTITY' not in x else x,
                    table_statement.replace('\n', '\n ').replace('(', '(\n ').replace(',', ',\n ').split(
                        ',')))).replace(',\n ', ',').replace('(\n ', '(').replace('\n ', '\n')
                if table_statement != table_statement_updated:
                    rslt_sql_script = rslt_sql_script.replace(table_statement, table_statement_updated)
                    if table_name in remove_identity_trigger:
                        rslt_sql_script = rslt_sql_script.replace(remove_identity_trigger[table_name], '')
                    if table_name in remove_identity_sequence:
                        rslt_sql_script = rslt_sql_script.replace(remove_identity_sequence[table_name], '')
    #lineNumberFunction(40, 0, 'Sequence and Trigger', 'IDENTITY column in Table creation script', logf, rslt_sql_script)
    nullidentity = r'NOT\s+NULL\s+GENERATED\s+BY\s+DEFAULT\s+AS\s+IDENTITY'
    nullidentitystr = re.compile(nullidentity, re.IGNORECASE | re.MULTILINE)
    nullidentityproc = re.finditer(nullidentitystr, rslt_sql_script)
    if (nullidentityproc):
        for nullstr in nullidentityproc:
            statement = nullstr.group()
            rslt_sql_script = rslt_sql_script.replace(statement, 'GENERATED BY DEFAULT AS IDENTITY NOT NULL')
    notnullidentity = r'NULL\s+GENERATED\s+BY\s+DEFAULT\s+AS\s+IDENTITY'
    notnullidentitystr = re.compile(notnullidentity, re.IGNORECASE | re.MULTILINE)
    notnullidentityproc = re.finditer(notnullidentitystr, rslt_sql_script)
    if (notnullidentityproc):
        for nullstr in notnullidentityproc:
            statement = nullstr.group()
            rslt_sql_script = rslt_sql_script.replace(statement, 'GENERATED BY DEFAULT AS IDENTITY NULL')
    return rslt_sql_script

def gttNamingConvention(rscript, logf):
    createProcedures, index = getPatternMatches(rscript, regdf, 30)
    if createProcedures:
        for createProcedure in createProcedures:
            initials = "".join([procedure[0] for procedure in createProcedure[4].split('_')])
            gttTableName, index = getPatternMatches(rscript, regdf, 31)
            j=0
            if(gttTableName):
                for gttTable in gttTableName:
                    gttEnrlTable = "{table_type}_{initials}{table_name}".format(table_type=gttTable[0], initials=initials, table_name=gttTable[1]).upper()
                    rscript = rscript.replace("".join(gttTable), gttEnrlTable)
                    lineNumberFunction(72, index[j], "".join(gttTable), gttEnrlTable, logf, rscript)
                    j = j+1
    return rscript

def comment_del_stmts(sql_script, logf):
    pattern = r'[\n\s]+delete.*?;'
    rslt_sql_script = sql_script[:]
    del_stmts = [(x.span()[0], x.span()[1], x.group()) for x in re.finditer(pattern, sql_script, re.I|re.DOTALL) if ' where ' not in re.sub(r'[\s\n]+--.*?[\n]', '', re.sub(r'\/\*.*?\*\/', '', re.sub(r'[\s\n]+',' ',x.group()))).lower()]
	#print(del_stmts)
    for del_stmt in sorted(del_stmts, reverse=True):
        lineNumberFunction(100, del_stmt[0], rslt_sql_script[del_stmt[0]:del_stmt[1]], del_stmt[2].replace('\n', '\n--'), logf, rslt_sql_script)
        rslt_sql_script = rslt_sql_script.replace(del_stmt[2], del_stmt[2].replace('\n', '\n--'))
    return rslt_sql_script


def dropProcStmt(rscript, logf):
    dropProcreg = r'(BEGIN\s+IF\s+)([\w\W]*)(\s+drop\s+procedure\s+)(.*)([^;]*)(\s*;\s*end;)'
    dropProcstr = re.compile(dropProcreg, re.IGNORECASE | re.MULTILINE)
    dropProcs = re.finditer(dropProcstr, rscript)
    if (dropProcs):
        for dropProc in dropProcs:
            statement = dropProc.group()
            lineNumberFunction(100, dropProc.span()[0], dropProc.group(), '',logf,rscript)
            rscript = rscript.replace(statement, '')

    return rscript

def datecolumn(rscript, logf, fileName):
    input_list = []

    input_file = file_folder + "\\input\\" + fileName
    input = open(input_file, "r").read()

    datetimecompile = r'(,)( *\s*)(.*)(\bDATETIME\b)'
    datetimestr = re.compile(datetimecompile, re.IGNORECASE | re.MULTILINE)
    datetimeproc = re.finditer(datetimestr, input)
    if (datetimeproc):
        for datetime in datetimeproc:
            input_list.append(datetime.group())

    datecompile = r'(,|;)(\s)(.*)(\bDATE\b)'
    datestr = re.compile(datecompile, re.IGNORECASE | re.MULTILINE)
    dateproc = re.finditer(datestr, rscript)
    if (dateproc):
        for date in dateproc:
            output_value = date.group()
            output_compare = re.sub(r'[^\w\s]', '', output_value)
            output_compare = re.sub(r'[\s+]', '', output_compare)
            for input_value in input_list:
                input_compare = re.sub(r'[^\w\s]', '', input_value)
                input_compare = re.sub(r'[\s+]', '', input_compare)
                if input_compare.upper() == output_compare.replace('DATE', 'DATETIME').upper().replace("V_", ""):
                    rscript = rscript.replace(output_value, output_value.replace('DATE', 'TIMESTAMP(3)'))
                    lineNumberFunction(11, date.span()[0], output_value,
                                       output_value.replace('DATE', 'TIMESTAMP(3)'), logf, rscript)
    return rscript

def removeExtraComma(rscript, logf):
    extraCommare = re.compile(r'(ALTER\s+TABLE\s+.*\s+)(ADD\s+)(.*,\s*.*key)', re.IGNORECASE | re.MULTILINE)
    extraCommafind = re.finditer(extraCommare, rscript)
    if (extraCommafind):
        for extraCommalist in extraCommafind:
            rscript = rscript.replace(extraCommalist.group(), extraCommalist.group().replace(",", ""))
            lineNumberFunction(100, extraCommalist.span()[0], extraCommalist.group(),
                               extraCommalist.group().replace(",", ""), logf, rscript)

    extraSpacere = re.compile(r'(DEFAULT\s+)([0-9]+)', re.IGNORECASE | re.MULTILINE)
    extraSpacefind = re.finditer(extraSpacere, rscript)
    if (extraSpacefind):
        for extraSpacelist in extraSpacefind:
            rscript = rscript.replace(extraSpacelist.group(), extraSpacelist.group()+" ")
            lineNumberFunction(100, extraSpacelist.span()[0], extraSpacelist.group(),
                               extraSpacelist.group()+" ", logf, rscript)

    return rscript

#-----------------Call Function which call other functions which update the input code-------------------#
def callFunctions(rscript, logf, fileName):
    pscript = pragmaFunction(rscript, logf)
    lrTrimScript = replaceLRtrims(pscript, logf)
    delIdScript = replaceDeleteIdentity(lrTrimScript, logf)
    ordersscript = replacePlusinOrderBy(delIdScript, logf) 
    wheresscript = replacePlusinWhere(ordersscript, logf)
    select21script = replaceSelect21(wheresscript, logf)  
    transactionScript = replaceTransactionCalls(select21script, logf)
    nullscript = replaceNulls(transactionScript, logf)
    strReplacescript = replaceStringReplaceFunction(nullscript, logf)
    sysColumncript = replacesysTable(strReplacescript, logf)
    setcommandscript = SETCOUNTFunction(sysColumncript,logf)
    addGTT = GTTNameFunction(setcommandscript, logf)
    replaceStatisticsString = replaceStatistics(addGTT,logf)
    replaceSysdate = SYSDATEFunction(replaceStatisticsString, logf)
    replaceChar = charFunction(replaceSysdate, logf)
    executeImmediatefuncstr = ExecuteImmediateFunction(replaceChar, logf)
    removedRepeatedDeleteStatements = remove_consecutive_dup_del_stmts(executeImmediatefuncstr, logf)
    commentSPCompile = comment_sp_recompile(removedRepeatedDeleteStatements, logf)
    identityColumnInCreateStatement = fix_identity_column_definition(commentSPCompile, logf)
    encloseWithBeginEndForSelectInto = enclose_sel_into(identityColumnInCreateStatement, logf)
    inOutParamNameScript = inOutParamName(encloseWithBeginEndForSelectInto, logf)
    footerScript = addFooter(inOutParamNameScript, logf, "ADMIN")
    gttNamingConventionScript = gttNamingConvention(footerScript, logf)
    commentDeleteScript = comment_del_stmts(gttNamingConventionScript, logf)
    headerScript = addHeader(commentDeleteScript, logf)
    dropPROcscript = dropProcStmt(headerScript, logf)
    removeSlashScript = removeSlash(dropPROcscript, logf)
    removeExtraCommaScript = removeExtraComma(removeSlashScript, logf)
    finalScript = datecolumn(removeExtraCommaScript, logf, fileName)
    return finalScript

#-----------------Main function calls above functions for all staged files-------------------#
def Main(path):
    #global script
    global counter
    counter = 0
    sqlFiles=glob.glob(path)
    for sqlFile in sqlFiles:
        fileName = sqlFile.split('\\')[-1]
        index_value = df[df['Object_Name']==fileName.upper()].index.values
        for i in index_value:
            df.iloc[i, df.columns.get_loc('Status')] = 'FACTORY MODEL Started'
        df.to_csv(file_folder + '\\logs\\status.csv')
        logPath = file_folder+'\\' + '\\logs\\logFile_'+fileName[:-4]+'.log'
        wikiPath = file_folder+'\\' + '\\logs\\refWikiFile_'+fileName[:-4]+'.log'
        with open(sqlFile, 'r') as file:
            script = file.read()
            rscript = script
            with open(logPath, 'w+') as logf:
                logf.write('Wiki#\tLine_Number\tOld_Code\tNew_Code\n')
                finalScript = callFunctions(rscript, logf, fileName)
                outputFilePath = outputPath + '/' + fileName
                outFile = open(outputFilePath, 'w')        
                outFile.write(finalScript)
                outFile.close()
                logf.close()
            with open(wikiPath, 'w+') as wikif:
                wikif.write("*********************The Factory Model couldn't convert the below points. Please work on them manually*********************\n")
                wikiFunctions(rscript, counter, wikif)
        for i in index_value:
            df.iloc[i, df.columns.get_loc('Status')] = 'FACTORY MODEL Completed'
        df.to_csv(file_folder + '\\logs\\status.csv')

#-----------------Read staged files and call Main function-------------------#
pathCapital = file_folder+'\\staging\\*.SQL'
capitalSQL = glob.glob(pathCapital)
for capitalFile in capitalSQL:
    base = os.path.splitext(capitalFile)[0]
    os.rename(capitalFile, base + '.sql')

outputPath = file_folder+'\\' + 'output'
path = file_folder+'\\staging\\*.sql'
Main(path)


#-------------------------SQLPLUS Function---------------------------#
def runSqlQuery(sqlCommand, connectString):
   session = Popen(['sqlplus', '-S', connectString], stdin=PIPE, stdout=PIPE, stderr=PIPE, cwd=code_folder + '\\instantclient_19_5', shell=True)
   (stdoutbytes, stderrbytes) = session.communicate(sqlCommand.encode('utf-8'))
   return (stdoutbytes.decode('utf-8'), stderrbytes.decode('utf-8'))

#---------------Execute table DDLs and Check status------------------#
for connection_row in connection_dict:
    filedata = open(outputPath + '\\' + connection_row['Object_name'].upper() + '.sql', 'r')
    execute_command = filedata.read().replace('SCHEMA_NAME', connection_row['Target_schema'])
    filedata = open(outputPath + '\\' + connection_row['Object_name'].upper() + '.sql', 'w')
    filedata.write(execute_command)
    filedata.close()

    conn_string = str(connection_row['Target_userid']) + '/' + str(
        connection_row['Target_password']) + '@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=' + str(
        connection_row['Target_server']) + ')(PORT=' + str(
        connection_row['Target_port']) + '))(CONNECT_DATA=(SID=' + str(
        connection_row['Target_SID']) + ')))'

    index_value = df[df['Object_Name'] == connection_row['Object_name'].upper() + '.sql'.upper()].index.values

    code_compiled = """SET HEADING OFF
                                SELECT * FROM ALL_OBJECTS where owner = \'""" + connection_row[
        'Target_schema'] + """\' and object_name = \'""" + connection_row['Object_name'].upper() + """\';"""
    queryResult = runSqlQuery(code_compiled, conn_string)

    validstr = re.compile(r'\b({0})\b'.format('VALID'), flags=re.IGNORECASE)
    if re.search(validstr, queryResult[0]):
        for i in index_value:
            df.iloc[i, df.columns.get_loc('Compiled(Y/N)')] = 'Error: Object already exists'
    else:
        queryResult, errorMessage = runSqlQuery(execute_command, conn_string)

        queryResult = runSqlQuery(code_compiled, conn_string)

        if re.search(validstr, queryResult[0]):
            for i in index_value:
                df.iloc[i, df.columns.get_loc('Compiled(Y/N)')] = 'Compiled'
        else:
            for i in index_value:
                df.iloc[i, df.columns.get_loc('Compiled(Y/N)')] = 'Not Compiled'

    df.to_csv(file_folder + '\\logs\\status.csv')


print('Script execution completed successfully.')


